from collections import defaultdict
import itertools
import os
import util

hamming_dict = defaultdict(dict)
codon_one_distance = defaultdict(set)
chemical_properties_dict = defaultdict(float)

theoretical_max_fitness = (9.0,1.0,9.0)

def prepare_chemical_properties_dict(file_path):

    global chemical_properties_dict

    temp_dict = {}

    with open(file_path,'rU') as fhandle:

        fhandle.readline()

        for line in fhandle:

            tokens = line.strip().split('\t')
            AA = tokens[0]
            chem = tokens[-1]

            temp_dict[AA] = chem

        temp_dict['X'] = 'stop'

    chemical_properties_dict = temp_dict

def build_codon_distance_dict(temp_aa_dict):

    global codon_one_distance

    codons = set()
    for aa in temp_aa_dict:
        codons.update(temp_aa_dict[aa])

    for (x,y) in itertools.product(codons, codons):
        if(hamming_distance(x,y) == 1):
            codon_one_distance[x].add(y)

def compute_distance_score(code, codon_to_aa):

    score = 0
    unique_aa_score = 0

    aa_visit_score = defaultdict(int)

    for codon_x in codon_to_aa:

        aa_accessible = set()

        for codon_y in codon_one_distance[codon_x]:

            if(codon_y not in codon_to_aa):
                continue
            elif(codon_to_aa[codon_x] != codon_to_aa[codon_y]):
                aa_accessible.add(codon_to_aa[codon_y])
                score+=1
                aa_visit_score[codon_to_aa[codon_y]]+=1

        unique_aa_score += len(aa_accessible)

    aa_score = float(unique_aa_score)/len(codon_to_aa.keys())
    aa_score = aa_score / theoretical_max_fitness[0]

    visiting_score = float(min(aa_visit_score.values()))/float(max(aa_visit_score.values()))
    visiting_score = visiting_score / theoretical_max_fitness[1]

    chemical_score,_ = compute_chemical_prop_distance(codon_to_aa)
    chemical_score = chemical_score / theoretical_max_fitness[2]

    return aa_score * visiting_score * chemical_score, \
               (aa_score, visiting_score, chemical_score)

def compute_distance_score_penalize_changes(code, codon_to_aa, original_code, power=1.0, max_replacements = 64):

    score, s_tuple = compute_distance_score(code, codon_to_aa)
    changes = sum([1 for x in original_code if original_code[x] != codon_to_aa[x]])
    fraction_diff = float(changes) / float(len(original_code.keys()))
    alpha = (1.0 + fraction_diff) ** power

    if(changes < max_replacements):
        score = score/alpha
        s_tuple = (s_tuple[0]/alpha, s_tuple[1]/alpha, s_tuple[2]/alpha, s_tuple[3]/alpha)
    else:
        score = 0
        s_tuple = (0, 0, 0, 0)

    return score, s_tuple

def compute_chemical_prop_distance(codon_to_aa):

    score = 0.0

    for codon_x in codon_to_aa:
        for codon_y in codon_one_distance[codon_x]:
            if(codon_y in codon_to_aa and codon_to_aa[codon_x] != codon_to_aa[codon_y]):
                aa_x = codon_to_aa[codon_x]
                aa_y = codon_to_aa[codon_y]
                if(chemical_properties_dict[aa_x] != chemical_properties_dict[aa_y]):
                    score+=1

    score = score / float(len(codon_to_aa.values()))

    return score, (1,1,score)

def hamming_distance(str1, str2):
    distance = 0
    if (len(str1) != len(str2)):
        raise AssertionError('unequal length in strings')

        # calculate hamming distance
    for base_x, base_y in zip(str1, str2):
        if (base_x != base_y):
            distance += 1

    return distance

def load_genetic_code(filename, exclude_stop=False):
    fhandle = open(filename, 'rU')

    fhandle.readline()
    aa_to_codon = defaultdict(list)

    code = {}

    for line in fhandle:

        tokens = line.strip().split('\t')
        codon = tokens[0]
        aa = tokens[1]

        if (exclude_stop):
            if (aa != 'X'):
                code[codon] = aa
                aa_to_codon[aa].append(codon)

        else:
            code[codon] = aa
            aa_to_codon[aa].append(codon)

    codon_product = itertools.product(code.keys(), code.keys())

    one_transitions = defaultdict(int)

    for (codon_x, codon_y) in codon_product:

        if (hamming_distance(codon_x, codon_y) == 1 and code[codon_x] != code[codon_y]):
            one_transitions[codon_x] += 1
            one_transitions[codon_y] += 1

    fhandle.close()

    return code, aa_to_codon, one_transitions

def best_move_for_code_recursive(code_fitness_id_tuple):

    import copy

    code = code_fitness_id_tuple[0]
    aa_to_codon = code_fitness_id_tuple[1]
    generation_ID = code_fitness_id_tuple[4]

    codons = code.keys()

    allowed_shifts = []

    for codon in codons:
        if(len(aa_to_codon[code[codon]]) > 1):
            allowed_shifts.append(codon)

    wt_score,wt_tuple = compute_distance_score(aa_to_codon, code)
    best_improvement_score = wt_score
    best_chem_score = wt_tuple[2]

    code_output = []

    for codon in allowed_shifts:

        for aa in aa_to_codon:

            if(aa == code[codon]):
                continue

            temp_aa_dict = copy.deepcopy(aa_to_codon)
            temp_code = copy.deepcopy(code)

            temp_aa_dict[aa].append(codon)
            temp_aa_dict[code[codon]].remove(codon)

            temp_code[codon] = aa

            #distance_dict = build_distance_dict(temp_aa_dict)

            score,mt_tuple = compute_distance_score(temp_aa_dict, temp_code)

            code_output.append((temp_code, temp_aa_dict, score, mt_tuple))

            if(score > best_improvement_score):
                best_improvement_score = score

            if(mt_tuple[2] > best_chem_score):
                best_chem_score = mt_tuple[2]

    filtered_codes = filter(lambda x: x[2] >= best_improvement_score,code_output)

    if(len(filtered_codes) == 0):
        filtered_codes.append((code, aa_to_codon, wt_score, wt_tuple))

    output = []
    for i in range(0, len(filtered_codes)):
        output.append((filtered_codes[i][0],
                       filtered_codes[i][1],
                       best_improvement_score,
                       best_chem_score,
                       generation_ID,
                       generation_ID + '.' + str(i)))

    return output

def build_recursive_code(codes_to_collect = set(), output_fitness_name = ''):

    wt_code = 'Genetic Code EC.txt'
    code,aa_to_codon,_ = load_genetic_code(os.path.join(util.output_sequence_dir(),wt_code),exclude_stop=True)

    recursive_code_name = 'RIC' + output_fitness_name

    cp = itertools.product(code.keys(), code.keys())
    for (x, y) in cp:
        hamming_dict[x][y] = hamming_distance(x, y)
        hamming_dict[y][x] = hamming_dict[x][y]

    build_codon_distance_dict(aa_to_codon)

    code_tuples = [(code, aa_to_codon, 0, '0', '0')]

    fitness_tracking_dict = dict()
    fitness_tracking_dict['0'] = 0
    final_code_collection = []

    current_best = 0
    best_chem_score = 0

    counter = 0

    wt_score,wt_tuple = compute_distance_score(aa_to_codon,code)

    codes_to_write = []

    while(len(code_tuples) > 0):

        ## edit into true recursive branching algorithm
        codes_to_process = []

        for code_tuple in code_tuples:
            codes_to_process.extend(best_move_for_code_recursive(code_tuple))

        code_tuples = []

        for (code,aa_to_codon,score,chem_score,progenitor,code_id) in codes_to_process:

            if(code_id in codes_to_collect):
                mt_score, mt_tuple = compute_distance_score(aa_to_codon,code)
                codes_to_write.append((code, aa_to_codon, score, mt_tuple, progenitor, code_id))
                print 'Collected %s' % code_id

            if(score > current_best):
                current_best = score

            if(chem_score > best_chem_score):
                best_chem_score = chem_score

        for (code,aa_to_codon,score,chem_score,progenitor,code_id) in codes_to_process:

            if(chem_score < best_chem_score):
                continue

            if(score == fitness_tracking_dict[progenitor]):

                mt_score, mt_tuple = compute_distance_score(aa_to_codon,code)
                final_code_collection.append((code,aa_to_codon,score,mt_tuple,progenitor,code_id))
                continue
            else:
                code_tuples.append((code,aa_to_codon,score,progenitor,code_id))
                fitness_tracking_dict[code_id] = score

        print len(code_tuples), 'codes left'

        counter+=1

    print 'Final codes collected: %i' % len(final_code_collection)

    output_codes = filter(lambda x: x[2] >= current_best, final_code_collection)

    print 'Codes surviving filter based on maximum detected fitness value: %i' % len(output_codes)
    print 'Number of generations: %i' % counter

    for (code, _, _, _, _, code_id) in output_codes:

        with open(os.path.join('ric_sequences', recursive_code_name + str(code_id) + '.txt'),'w') as fhandle:

            fhandle.write('Codon\tAA\tAbundance\n')

            for key in code:
                fhandle.write(key + '\t' + code[key] + '\t1.0\n')

            fhandle.write('TAA' + '\t' + 'X\t1.0\n')
            fhandle.write('TAG' + '\t' + 'X\t1.0\n')
            fhandle.write('TGA' + '\t' + 'X\t1.0\n')

    fitness_array = []

    for (code, _, score, fit_tuple, _, code_id) in codes_to_write:

        fitness_array.append((score, fit_tuple[0], fit_tuple[1], fit_tuple[2]))

        '''

        with open(os.path.join(util.output_sequence_dir(), recursive_code_name + str(code_id) + '.txt'),'w') as fhandle:

            fhandle.write('Codon\tAA\tAbundance\n')

            for key in code:
                fhandle.write(key + '\t' + code[key] + '\t1.0\n')

            fhandle.write('TAA' + '\t' + 'X\t1.0\n')
            fhandle.write('TAG' + '\t' + 'X\t1.0\n')
            fhandle.write('TGA' + '\t' + 'X\t1.0\n')

        '''

    if(len(fitness_array)>0):

        with open(os.path.join(util.output_sequence_dir(),recursive_code_name+'_fitness_array.txt'),'w') as fhandle:
            fhandle.write('\t'.join(['Fitness','Unique AAs','Min Max Codon Ratio','Chemical Diversity Variance']) +'\n')
            for y in fitness_array:
                fhandle.write('\t'.join(map(str,y)) + '\n')

def driver():

    prepare_chemical_properties_dict(os.path.join(util.input_data_dir(),'aa_properties.txt'))

    code_lineage_ids = ['0.0.1.0.1.2.0.3.0.1.0.0.0.0.0.0.0',
                        '0.1.1.0.1.2.0.3.0.0.0.0.0.0.0.0.0']

    for code_lineage_id in code_lineage_ids:

        codes_to_write = set()

        ids = code_lineage_id.split('.')
        for i in range(0,len(ids)):
            codes_to_write.add('.'.join(ids[0:i+1]))

        build_recursive_code(codes_to_write, output_fitness_name=code_lineage_id)
    #build_recursive_code()

if(__name__ == '__main__'):

    driver()
    



