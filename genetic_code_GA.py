__author__ = 'jdwinkler'
from collections import defaultdict
import itertools
import random as random
import os
import util
import math

hamming_dict = defaultdict(dict)
chemical_properties_dict = defaultdict(float)
codon_one_distance = defaultdict(set)

theoretical_max_fitness = (9.0,1.0,9.0)


class Codon:

    """

    Represents a codon within a GeneticCode object. Mainly helps with bookkeeping.

    """

    def __init__(self, codon, amino_acid, native, moveable=True):
        self.native = native
        self.original_amino_acid = amino_acid
        self.current_amino_acid = amino_acid
        self.codon = codon

        self.hash_code = hash(self.codon)

        self.moveable = moveable

    def reassign(self, new_aa):
        native = self.original_amino_acid == new_aa
        current_amino_acid = new_aa

        cc = Codon(self.codon, self.original_amino_acid, native)
        cc.current_amino_acid = current_amino_acid

        return cc

    def __hash__(self):
        return self.hash_code

    def __eq__(self, other):
        return self.codon == other.codon

    def __repr__(self):
        return self.codon


class GeneticCode:

    """

    An immutable [as much as is possible in python] representation of a genetic code.


    """

    def __init__(self, aa_to_codon_dict, original_dict=None, swapped_aa=None):



        # aa_to_codon dict maps AA to codon objects
        self.code = aa_to_codon_dict.copy()
        self.keys = aa_to_codon_dict.keys()

        self.codons = set()

        for c in aa_to_codon_dict.values():
            self.codons.update(c)


        #can re-use distance calculations for each AA dict-saves a significant amount of time.
        if (original_dict == None):
            self.distance_dict = defaultdict(dict)
        else:
            self.distance_dict = defaultdict(dict)
            for key in original_dict:
                self.distance_dict[key] = original_dict[key].copy()

        #swapped aa is a (aa, aa') list that documents which AAs have had codons swapped
        if (swapped_aa == None):
            iter_set = itertools.product(aa_to_codon_dict.keys(), aa_to_codon_dict.keys())
        else:
            iter_set = [(x, y) for x in self.keys for y in swapped_aa]

        for (x, y) in iter_set:

            if (x == y):
                cp = itertools.product(aa_to_codon_dict[x], aa_to_codon_dict[x])

                min_dist = 3

                for (codon1, codon2) in cp:
                    if (codon1 == codon2):
                        continue
                    min_dist = min(hamming_dict[codon1.codon][codon2.codon], min_dist)
                self.distance_dict[x][x] = min_dist
                # print self.distance_dict[x][x]

            else:
                self.distance_dict[x][y] = min([hamming_dict[cp[0].codon][cp[1].codon] for cp in
                                                itertools.product(aa_to_codon_dict[x], aa_to_codon_dict[y])])
                self.distance_dict[y][x] = self.distance_dict[x][y]

                # self.transition_dict = one_transitions

        self.codon_to_aa = dict()
        for aa in self.code:
            for codon in self.code[aa]:
                self.codon_to_aa[codon.codon] = aa

    def swap(self, partner_code, relax_native_restriction=False):

        new_code = defaultdict(list)

        codons_self = set()
        codons_part = set()

        for aa in self.code:
            codons_self.update(self.code[aa])
            codons_part.update(partner_code.code[aa])

        # not handling this case (complicated)
        if (len(codons_part - codons_self) > 0):
            return GeneticCode(self.code, original_dict=self.distance_dict)

        assigned_codons = set()

        base_code = self.code if random.random() < 0.5 else partner_code.code

        for aa in base_code:

            # assign one codon from either code randomly
            choices = []
            choices.extend(base_code[aa])

            temp = []
            for codon in choices:
                if codon not in assigned_codons:
                    temp.append(codon)

            choices = temp
            if (not relax_native_restriction):

                choices = [x for x in choices if x.native]

            try:
                codon = random.choice(choices)
                new_code[aa].append(codon)
                assigned_codons.add(codon)
            except:
                print 'EXCEPTION'
                print choices
                print assigned_codons
                print aa
                print self.code[aa]
                print partner_code.code[aa]
                raise

        # okay, every aa has at least one codon now. take the rest of the codons
        # and randomly assign them to their amino acids
        self_codon_to_aa = {}
        part_codon_to_aa = {}

        for aa in self.code:
            for codon in self.code[aa]:
                self_codon_to_aa[codon] = aa

        for aa in partner_code.code:
            for codon in partner_code.code[aa]:
                part_codon_to_aa[codon] = aa

        remaining_codons = codons_self - assigned_codons

        for codon in remaining_codons:

            if (random.random() < 0.5):
                new_code[self_codon_to_aa[codon]].append(codon.reassign(self_codon_to_aa[codon]))
            else:
                new_code[part_codon_to_aa[codon]].append(codon.reassign(part_codon_to_aa[codon]))

            assigned_codons.add(codon)

        if (len(assigned_codons) != len(codons_self) or len(assigned_codons) != len(codons_part)):
            raise AssertionError('Missing codons in swapping function?')

        return GeneticCode(new_code)

    def shuffle(self, p, relax_native_restriction=False, can_delete_aa=False):

        new_aa_to_codon_dict = defaultdict(list)

        max_codons = 64

        changed_aa = []

        for i in range(0, len(self.keys)):

            # check to see how many codons remain. if 1, skip.
            if (len(self.code[self.keys[i]]) == 1):
                new_aa_to_codon_dict[self.keys[i]].extend(self.code[self.keys[i]])
                continue

            if (random.random() < p):
                # exclude self, codons exceeeding max codon limitations
                random_choices = [k for k in range(0, len(self.keys)) if k != i]

                # aa to receive one of the current aa's codons
                receiver = random.choice(random_choices)

                # get current set of codons
                current_codons = self.code[self.keys[i]]

                # okay, logic: I want every amino acid to have at least one unchanged, native codon remaining
                # (I don't care which.)
                # need to check current codons for count of native codons.
                # if this equals 1, exclude that codon from random donation to receipient codon receiver.
                # if this count >1, pick randomly from entire subset.

                allowed_native_count = 0
                allowed_native_position = 0
                immoveable_native_count = 0

                allowed_indices = [k for k in range(0, len(current_codons)) if current_codons[k].moveable]
                immovable_indices = [k for k in range(0, len(current_codons)) if not current_codons[k].moveable]

                if(len(allowed_indices) == 0):
                    new_aa_to_codon_dict[self.keys[i]].extend([current_codons[k] for k in immovable_indices])
                    continue

                for j in allowed_indices:
                    if (current_codons[j].native):
                        allowed_native_count += 1
                        allowed_native_position = j

                for j in immovable_indices:

                    if (current_codons[j].native):
                        immoveable_native_count += 1

                if (allowed_native_count + immoveable_native_count > 1 or relax_native_restriction):
                    donated_codon_index = random.choice(allowed_indices)
                    allowed_indices.remove(donated_codon_index)
                elif (allowed_native_count == 1 and immoveable_native_count == 0):
                    allowed_indices.remove(allowed_native_position)
                    donated_codon_index = random.choice(allowed_indices)
                    allowed_indices.remove(donated_codon_index)
                    allowed_indices.append(allowed_native_position)
                elif (allowed_native_count == 0 and immoveable_native_count == 1):
                    donated_codon_index = random.choice(allowed_indices)
                    allowed_indices.remove(donated_codon_index)
                else:
                    raise AssertionError('Should always have at least one native codon for each amino acid.')

                codon = current_codons[donated_codon_index]
                reassigned_codon = codon.reassign(self.keys[receiver])

                keep_aa = False

                if (not can_delete_aa):
                    keep_aa = True
                elif (random.random() <= 0.8):
                    keep_aa = True

                if (keep_aa):
                    new_aa_to_codon_dict[self.keys[receiver]].append(reassigned_codon)

                    changed_aa.append(self.keys[receiver])
                    changed_aa.append(self.keys[i])

                new_aa_to_codon_dict[self.keys[i]].extend([current_codons[k] for k in allowed_indices])
                new_aa_to_codon_dict[self.keys[i]].extend([current_codons[k] for k in immovable_indices])

            else:

                # add all codons from old to new aa_to_codon dict
                new_aa_to_codon_dict[self.keys[i]].extend(self.code[self.keys[i]])

        if (not can_delete_aa):

            values = new_aa_to_codon_dict.values()
            codons = set()
            for c in values:
                codons.update(c)

        return GeneticCode(new_aa_to_codon_dict, original_dict=self.distance_dict, swapped_aa=changed_aa)

    @staticmethod
    def get_allowed_fitness_functions():

        return {'distance','linear','power', 'chemical'}

    def fitness_function_dispatcher(self, function_name, kwargs):

        allowed_names = GeneticCode.get_allowed_fitness_functions()

        if(function_name not in allowed_names):
            raise AssertionError('Unknown fitness function %s' % function_name)

        if(function_name == 'distance'):
            return self.compute_distance_score()
        if(function_name == 'linear'):
            return self.compute_distance_score_penalize_changes(kwargs['original_code'])
        if(function_name == 'power'):
            return self.compute_distance_score_penalize_changes(kwargs['original_code'],
                                                                power=kwargs['power'],
                                                                max_replacements=kwargs.get('max',64))
        if(function_name == 'chemical'):
            return self.compute_chemical_prop_distance()

    def compute_distance_score(self):

        score = 0
        unique_aa_score = 0

        aa_visit_score = defaultdict(int)

        for codon_x in self.codon_to_aa:

            aa_accessible = set()

            for codon_y in codon_one_distance[codon_x]:

                if(codon_y not in self.codon_to_aa):
                    continue
                elif(self.codon_to_aa[codon_x] != self.codon_to_aa[codon_y]):
                    aa_accessible.add(self.codon_to_aa[codon_y])
                    score+=1
                    aa_visit_score[self.codon_to_aa[codon_y]]+=1

            unique_aa_score += len(aa_accessible)

        aa_score = float(unique_aa_score)/len(self.codon_to_aa.keys())
        aa_score = aa_score / theoretical_max_fitness[0]

        visiting_score = float(min(aa_visit_score.values()))/float(max(aa_visit_score.values()))
        visiting_score = visiting_score / theoretical_max_fitness[1]

        chemical_score,_ = self.compute_chemical_prop_distance()
        chemical_score = chemical_score / theoretical_max_fitness[2]

        return aa_score * visiting_score * chemical_score, \
               (aa_score, visiting_score, chemical_score)

    def compute_distance_score_penalize_changes(self, original_code, power=1.0, max_replacements = 64):

        score, s_tuple = self.compute_distance_score()

        changes = sum([1 for x in original_code if original_code[x] != self.codon_to_aa[x]])

        fraction_diff = float(changes) / float(len(original_code.keys()))

        alpha = (1.0 + fraction_diff) ** power

        if(changes <= max_replacements):

            score = score/alpha
            s_tuple = (s_tuple[0], s_tuple[1], s_tuple[2])

        else:

            score = 0
            s_tuple = (0, 0, 0)

        return score, s_tuple

    def compute_chemical_prop_distance(self):

        score = 0.0
        for codon_x in self.codon_to_aa:
            for codon_y in codon_one_distance[codon_x]:
                if(codon_y in self.codon_to_aa and self.codon_to_aa[codon_x] != self.codon_to_aa[codon_y]):
                    aa_x = self.codon_to_aa[codon_x]
                    aa_y = self.codon_to_aa[codon_y]
                    if(chemical_properties_dict[aa_x] != chemical_properties_dict[aa_y]):
                        score+=1

        score = score / float(len(self.codon_to_aa.keys()))

        return score, (1,1,score)

def run_evolution_experiment(source_genetic_code, mutation_rate, generations, population_size, frozen_set=set(),
                             unchanged_limit=5, relax_native_restriction=False, can_delete=False, fit_f='distance',
                             additional_parameters = None):

    """

    Runs the simulation.

    :param source_genetic_code:
    :param mutation_rate:
    :param generations:
    :param population_size:
    :param frozen_set: codons that cannot be moved to new AAs
    :param unchanged_limit:
    :param relax_native_restriction:
    :param can_delete:
    :param fit_f:
    :param additional_parameters:
    :return:
    """

    codon_obj_dict = {}

    fitness_functions = GeneticCode.get_allowed_fitness_functions()

    if (fit_f not in fitness_functions):
        raise AssertionError('Unknown fitness function request: %s' % fit_f)

    for aa in source_genetic_code:
        codon_obj_dict[aa] = [Codon(x, aa, True, moveable=x not in frozen_set) for x in source_genetic_code[aa]]

    # build populations from provided source genetic code
    population = [GeneticCode(codon_obj_dict) for i in range(0, population_size)]

    original_code = GeneticCode(codon_obj_dict)

    codon_to_aa = dict()
    for aa in original_code.code:
        for codon in original_code.code[aa]:
            codon_to_aa[codon] = aa

    wt_fitness, wt_fit_tuple = original_code.fitness_function_dispatcher(fit_f,kwargs=additional_parameters)

    th_fitness = theoretical_max_fitness[0] * \
                 theoretical_max_fitness[1] * \
                 theoretical_max_fitness[2]

    print 'WT fitness values', wt_fit_tuple
    print 'WT fitness values normed', (
                                       wt_fit_tuple[0]/theoretical_max_fitness[0],
                                       wt_fit_tuple[1]/theoretical_max_fitness[1],
                                       wt_fit_tuple[2]/theoretical_max_fitness[2])

    # generate initial variability
    population = [x.shuffle(mutation_rate, relax_native_restriction=relax_native_restriction, can_delete_aa=can_delete)
                  for x in population]

    unchanged_counter = 0
    i = 0

    tracker_output = []

    previous_chemical_div = 0

    while (i < generations):

        fitness_array = []
        fitness_tuple = None

        max_fitness_position = 0
        max_fitness = 0
        temp_chemical_div = 0

        for j in range(0, len(population)):

            fitness,x = population[j].fitness_function_dispatcher(fit_f,kwargs=additional_parameters)
            fitness = fitness

            fitness_array.append((j, fitness, x[2]))

            if (fitness > max_fitness):
                max_fitness = fitness
                max_fitness_position = j
                fitness_tuple = (max_fitness, x[0],x[1],x[2])

            if(x[2] > previous_chemical_div):
                temp_chemical_div = x[2]

        tracker_output.append(fitness_tuple)

        # done! pick winners
        fitness_array_temp = filter(lambda x: x[2] >= previous_chemical_div, fitness_array)

        if(len(fitness_array_temp) > 0):
            fitness_array = fitness_array_temp

        fitness_array = sorted(fitness_array, key=lambda x: x[1], reverse=True)
        previous_chemical_div = temp_chemical_div

        print 'Generation %i best fitness %f at current mutation rate: %f' % (i, fitness_array[0][1], mutation_rate)

        if (unchanged_counter == unchanged_limit):
            i = generations - 1

        if (i + 1 != generations):

            limit = 10

            slice = [population[x[0]] for x in fitness_array[0:population_size / limit]]

            # try to break out of fitness valleys through mating
            new_pop = []
            swapped = []

            breeders = itertools.cycle(slice)
            while (len(new_pop) < population_size - len(swapped)):
                new_pop.append(breeders.next().shuffle(mutation_rate, relax_native_restriction=relax_native_restriction,
                                                       can_delete_aa=can_delete))
            new_pop.append(population[max_fitness_position])
            new_pop.extend(swapped)
            population = new_pop
        else:
            output_pop = []
            for (j, fitness,_) in fitness_array:
                output_pop.append((population[j], fitness))
            population = output_pop
            break

        i += 1

    # mutagenized genetic codes
    return population, tracker_output


def evaluate(output_name, code_file, mutation_rate, exclude_stop, relax_native, fitness_function,
             additional_parameters = None, immoveable_set = set()):

    """

    Generates genetic codes described in Pines and Winkler [2016]

    :param output_name:
    :param code_file:
    :param mutation_rate:
    :param exclude_stop:
    :param relax_native:
    :param fitness_function:
    :param additional_parameters:
    :return:
    """

    code, aa_to_codon, most_abundant_codons = load_genetic_code(code_file, exclude_stop=exclude_stop)

    if (exclude_stop and 'TAG' not in code and 'TGA' not in code):
        aa_to_codon['N'].extend(['TAG', 'TGA'])
        code['TAG'] = 'N'
        code['TGA'] = 'N'

    cp = itertools.product(code.keys(), code.keys())
    for (x, y) in cp:
        hamming_dict[x][y] = hamming_distance_real(x, y)
        hamming_dict[y][x] = hamming_dict[x][y]

    for codon_x in hamming_dict:
        for codon_y in hamming_dict[codon_x]:
            if(hamming_dict[codon_x][codon_y] == 1):
                codon_one_distance[codon_x].add(codon_y)

    generations = 2500
    individuals = 1000

    #immoveable_set = set()

    final_population, tracker_output = run_evolution_experiment(aa_to_codon, mutation_rate, generations, individuals,
                                                frozen_set=immoveable_set,
                                                unchanged_limit=generations, relax_native_restriction=relax_native,
                                                fit_f=fitness_function, additional_parameters=additional_parameters)

    (best_code, best_fitness) = final_population[0]

    print 'Max fitness (relative to native E. coli genetic code): %f' % best_fitness

    fhandle = open(output_name, 'w')
    fhandle.write('\t'.join(['Codon', 'AA', 'Frequency']) + '\n')

    for aa in best_code.keys:

        for x in best_code.code[aa]:
            fhandle.write('\t'.join([x.codon, aa, str(1.0)]) + '\n')

    if ('X' not in best_code.keys):
        fhandle.write('\t'.join(['TAA', 'X', str(1.0)]) + '\n')

    fhandle.close()

    #(accessibility_score, aa_score, codon_score, chemical_score)

    with open(output_name.replace('.txt','_FitnessArray.txt'),'w') as f:

        f.write('\t'.join(['Generation', 'Fitness','Unique AAs','Min Max Codon Ratio','Chemical Diversity Variance']) + '\n')

        for i, ft in zip(range(0,len(tracker_output)),tracker_output):
            f.write(str(i) + '\t' + '\t'.join([str(x) for x in ft]) + '\n')

    print 'Done with %s!' % output_name

    return best_fitness

def prepare_chemical_properties_dict(file_path):

    temp_dict = {}

    with open(file_path,'rU') as fhandle:

        fhandle.readline()

        for line in fhandle:

            tokens = line.strip().split('\t')
            AA = tokens[0]
            chemical_class = tokens[-1]

            temp_dict[AA] = chemical_class

        temp_dict['X'] = 'stop'

    return temp_dict

def load_genetic_code(filename, exclude_stop=False):
    fhandle = open(filename, 'rU')

    fhandle.readline()

    aa_to_codon = defaultdict(list)

    code = {}

    max_frequency = defaultdict(list)

    for line in fhandle:

        tokens = line.strip().split('\t')
        codon = tokens[0]
        aa = tokens[1]
        frequency = float(tokens[2])

        if (not exclude_stop or (exclude_stop and aa != 'X')):
            code[codon] = aa
            aa_to_codon[aa].append(codon)
            max_frequency[aa].append((codon, frequency))

    freq_output = {}

    for aa in max_frequency:
        farray = sorted(max_frequency[aa], key=lambda x: x[1], reverse=True)
        freq_output[aa] = farray[0][0]

    return code, aa_to_codon, freq_output


def hamming_distance(str1, str2):
    return hamming_dict[str1][str2]


def hamming_distance_real(str1, str2):
    if (len(str1) != len(str2)):
        raise AssertionError('unequal length in strings')

    distance = 0
    for i in range(0, len(str1)):
        if (str1[i] != str2[i]):
            distance += 1
    return distance

def driver():

    global chemical_properties_dict

    chemical_properties_dict = prepare_chemical_properties_dict(os.path.join(util.input_data_dir(),'aa_properties.txt'))

    random.seed(456789)

    filename = os.path.join(util.input_data_dir(),'Genetic Code EC.txt')

    evaluate(os.path.join(util.output_sequence_dir(), 'Fixed ATG, No Native Restriction, Distance Fitness Function.txt'), filename,
             mutation_rate=0.10, exclude_stop=True, relax_native=True, fitness_function='distance')

    evaluate(os.path.join(util.output_sequence_dir(), 'Fixed ATG, Native Restriction, Distance Fitness Function.txt'), filename,
             mutation_rate=0.10, exclude_stop=True, relax_native=False, fitness_function='distance')

    wt_code, _, _ = load_genetic_code(filename,exclude_stop=True)
    object_code = dict()
    for codon in wt_code:
        object_code[codon] = wt_code[codon]

    params = {'original_code':object_code,
              'power' : 1.0}

    evaluate(
        os.path.join(util.output_sequence_dir(), 'Fixed ATG, Native Restriction, Linear Penalty for Changes Fitness Function.txt'),
        filename, mutation_rate=0.10, exclude_stop=True, relax_native=False, fitness_function='linear',
    additional_parameters=params)

    params['power'] = 2.0
    params['max'] = 7

    evaluate(
        os.path.join(util.output_sequence_dir(), 'Fixed ATG, Native Restriction 2.0 power penalty for Changes Fitness Function.txt'),
        filename, mutation_rate=0.10, exclude_stop=True, relax_native=False, fitness_function='power',
    additional_parameters=params)

    '''
    Design, synthesis, and testing toward a 57-codon genome codons:
    AGA,
    AGG,
    AGC,
    AGT,
    TTA,
    TTG,
    TAG
    '''

    wt_code, _, _ = load_genetic_code(filename,exclude_stop=False)
    object_code = dict()
    for codon in wt_code:
        object_code[Codon(codon,wt_code[codon],True,moveable=False)] = wt_code[codon]

    removed_church_codons = {'AGA','AGG','AGC','AGT','TTA','TTG','TAG'}
    fixed_codons = set([str(x) for x in wt_code.keys()]) - removed_church_codons

    evaluate(os.path.join(util.output_sequence_dir(), 'Ostgov Reassigned Strain Distance Fitness Function.txt'), filename,
             mutation_rate=0.10, exclude_stop=False, relax_native=True, fitness_function='distance', immoveable_set=fixed_codons)

if (__name__ == '__main__'):

    driver()

