from collections import defaultdict
import subprocess
import multiprocessing
from multiprocessing import Queue
import itertools
import util
import os
import platform


class RNAFoldWorker(multiprocessing.Process):
    def __init__(self, oq):

        super(RNAFoldWorker, self).__init__()

        self.input_queue = Queue()
        self.output_queue = oq

    def add_sequence(self, sequence):

        self.input_queue.put(sequence)

    def run(self):

        rnafold_name = 'rnafold.exe' if platform.system() == 'Windows' else 'rnafold'

        p = subprocess.Popen([rnafold_name, '--noPS'], stdout=subprocess.PIPE, stdin=subprocess.PIPE)

        processed = 0

        while (True):

            processed += 1

            sequence = self.input_queue.get(block=True)

            if (sequence == None):
                break

            p.stdin.write(sequence + '\n')

            outputList = []

            counter = 0
            for line in iter(p.stdout.readline, b''):

                counter += 1
                outputList.append(line.strip())

                if (counter == 2):
                    break

            sequence = outputList[0]
            secondary_structure = outputList[1].split(' ')[0]
            free_energy = outputList[1].split(' ', 1)[1].replace('(', '').replace(')', '').strip()

            self.output_queue.put((sequence, secondary_structure, float(free_energy)))

        p.terminate()

        return


def generate_MT_workers(threads):
    oq = Queue()

    workers = [RNAFoldWorker(oq) for i in range(0, threads)]

    for worker in workers:
        worker.start()

    return workers, oq


def compute_structure_properties_MT(workers, oq, input_sequences):
    output = []

    cycler = itertools.cycle(workers)

    counter = 0

    for seq in input_sequences:
        cycler.next().add_sequence(seq)
        counter += 1

    while (len(output) < len(input_sequences)):
        output.append(oq.get(block=True))

    return output


def compute_structure_properties(input_sequences):

    if (type(input_sequences) == str):
        input_sequences = [input_sequences]

    p = subprocess.Popen(['rnafold.exe', '--noPS'], stdout=subprocess.PIPE, stdin=subprocess.PIPE)
    output = []

    for sequence in input_sequences:

        p.stdin.write(sequence + '\n')

        outputList = []

        counter = 0
        for line in iter(p.stdout.readline, b''):

            counter += 1
            outputList.append(line.strip())

            if (counter == 2):
                break

        sequence = outputList[0].replace('U', 'T')
        secondary_structure = outputList[1].split(' ')[0]
        free_energy = outputList[1].split(' ', 1)[1].replace('(', '').replace(')', '').strip()

        output.append((sequence, secondary_structure, float(free_energy)))

    p.terminate()

    return output


def reverse_complement(sequence):

    mapper = {'T': 'A', 'A': 'T', 'G': 'C', 'C': 'G'}

    rc = []

    for i in range(len(sequence) - 1, 0, -1):
        rc.append(mapper[sequence[i]])

    return ''.join(rc)


def load_fasta(name):
    fhandle = open(name, 'rU')
    lines = fhandle.readlines()
    fhandle.close()

    gene_sequence_dict = defaultdict(list)
    current_key = ''

    for i in range(0, len(lines)):

        line = lines[i].strip()

        if ('>' in line):
            current_key = line
        elif (current_key != ''):
            gene_sequence_dict[current_key].append(line)

    return gene_sequence_dict


def generate_truncated_orfs(gene_sequence_dict, genome_sequence):

    output = {}
    failed_count = 0

    for key in gene_sequence_dict:

        output[key] = ''.join(gene_sequence_dict[key])[0:39]

        index = genome_sequence.find(output[key])
        rc_ed = False

        if (index == -1):
            index = genome_sequence.find(reverse_complement(output[key]))
            rc_ed = True

        # comes from Coding-Sequence Determinants of Gene Expression in Escherichia coli

        if (index == -1):
            failed_count += 1
            continue
        elif (rc_ed):
            output[key] = reverse_complement(
                genome_sequence[index + len(output[key]) - 1:index + 4 + len(output[key])]) + output[key]
        else:
            output[key] = genome_sequence[index - 4:index] + output[key]

    return output

def regenerate_truncated_orfs():

    ec_fasta = os.path.join(util.input_data_dir(),'essential_EColi_ORFs.fasta')

    genome_fasta = util.load_fasta(os.path.join(util.input_data_dir(),'ecoli_genome.fasta')).values()[0]

    truncated_sequences = generate_truncated_orfs(util.load_fasta(ec_fasta),genome_fasta)

    fhandle = open(os.path.join(util.output_sequence_dir(),'truncated_essential_ecoli_orfs.fasta'),'w')

    for key in truncated_sequences:
        fhandle.write(key + '\n')
        fhandle.write(truncated_sequences[key] + '\n')

    fhandle.close()

