# README #

The standard genetic code is robust to mutations during transcription and translation. Point mutations are likely to be synonymous or to preserve the chemical properties of the original amino acid. Saturation mutagenesis experiments suggest that in some cases the best performing mutant requires a replacement of more than a single nucleotide within a codon. These replacements are essentially inaccessible to common error-based laboratory engineering techniques that alter single nucleotide per mutation event, due to the extreme rarity of adjacent mutations. In this theoretical study, we suggest a radical reordering of the genetic code that maximizes the mutagenic potential of single nucleotide replacements. We explore several possible genetic codes that allow a greater degree of accessibility to the mutational landscape and may result in a hyper-evolvable organism serving as an ideal platform for directed evolution experiments. We then conclude by evaluating the challenges of constructing such a recoded organism and their potential applications within the synthetic biology field.

### How do I get set up? ###

You should be able to download and run the software as is if you have Python 2.7 installed with the necessary packages. The RNAfold extension may need to be replaced with a binary compiled for your particular OS/processor.

### Who do I talk to? ###

Gur Pines (Postdoctoral Researcher, CU-Boulder): gur.pines@colorado.edu
James Winkler (Developer/Biodomain Researcher): james.winkler@gmail.com