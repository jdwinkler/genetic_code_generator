# selects optimal codon usage for novel genetic codes to minimize (wt-novel) Delta G_folding
from collections import defaultdict
import rnafold_interface
import os
import itertools
import util


def load_code_structure_output(name):
    fhandle = open(name, 'rU')
    lines = fhandle.readlines()
    fhandle.close()

    gene_sequence_dict = {}

    for i in range(0, len(lines)):

        line = lines[i].strip()

        if ('>' in line):
            current_key = line

            sequence = lines[i + 1].strip()
            structure_fe = lines[i + 2].strip().split(' ')

            structure = structure_fe[0].strip()
            free_energy = float(structure_fe[1].replace('(', '').replace(')', ''))

            gene_sequence_dict[current_key] = (sequence, structure, free_energy)

    return gene_sequence_dict


def load_fasta(name):
    fhandle = open(name, 'rU')
    lines = fhandle.readlines()
    fhandle.close()

    gene_sequence_dict = defaultdict(list)
    current_key = ''

    for i in range(0, len(lines)):

        line = lines[i].strip()

        if ('>' in line):
            current_key = line
        elif (current_key != ''):
            gene_sequence_dict[current_key].append(line)

    for x in gene_sequence_dict:
        gene_sequence_dict[x] = ''.join(gene_sequence_dict[x])

    return gene_sequence_dict


def load_code(code_file):
    codon_to_aa = {}
    aa_to_codon = defaultdict(list)

    for line in open(code_file, 'rU'):
        if ('Codon' in line):
            continue

        tokens = line.strip().split('\t')

        codon = tokens[0]
        aa = tokens[1]

        codon_to_aa[codon.upper()] = aa.upper()
        aa_to_codon[aa.upper()].append(codon.upper())

    return codon_to_aa, aa_to_codon


def hamming_distance(str1, str2):
    # assert len(str1) == len(str2)

    return sum([1 if (str1[i] != str2[i]) else 0 for i in range(0, len(str1))])


def tweak_whole_sequence(target_sequence, original_sequence, proposed_aa_to_codons, novel_code, workers, output_queue,
                         starting_position=0):

    # secondary structure can differ in 10% of places (or best possible)
    ss_delta_tolerance = 0.1 * float(len(target_sequence))

    seq_comparison = rnafold_interface.compute_structure_properties([original_sequence, target_sequence])

    (_, original_structure, original_dG) = seq_comparison[0]
    (_, target_structure, target_dG) = seq_comparison[1]

    original_structure_triplets = [original_structure[i:i + 3] for i in
                                   range(starting_position, len(original_structure), 3)]
    target_structure_triplets = [target_structure[i:i + 3] for i in range(starting_position, len(target_structure), 3)]

    differing_structure_codon_indices = set()

    for o_triplet, t_triplet, i in zip(original_structure_triplets, target_structure_triplets,
                                       range(0, len(original_structure_triplets))):

        if (hamming_distance(o_triplet, t_triplet) > 2):
            differing_structure_codon_indices.add(i)

    target_codons = [target_sequence[i:i + 3] for i in range(starting_position, len(target_sequence), 3)]

    if (starting_position != 0):
        candidate_sequence_leader = target_sequence[0:starting_position]
    else:
        candidate_sequence_leader = ''

    proposed_sequence = []

    for i in range(0, len(target_codons)):

        if (i not in differing_structure_codon_indices):
            proposed_sequence.append([target_codons[i]])
        else:
            proposed_sequence.append(proposed_aa_to_codons[novel_code[target_codons[i]]])

    seq_iterator = itertools.product(*proposed_sequence)

    sequence_compiler = []
    counter = 0

    for x in seq_iterator:
        sequence_compiler.append(candidate_sequence_leader + ''.join(x))
        counter += 1

    results = rnafold_interface.compute_structure_properties_MT(workers, output_queue, sequence_compiler)

    results.append((target_sequence, target_structure, target_dG))

    best_delta = hamming_distance(original_structure, target_structure)
    ddG = (target_dG - original_dG) ** 2
    best_dG = target_dG
    best_sequence = target_sequence
    best_ss = target_structure

    for (seq, secondary_structure, dG) in results:

        distance = hamming_distance(secondary_structure, original_structure)
        delta_gibbs = (dG - original_dG) ** 2

        if (distance <= best_delta and delta_gibbs < ddG):
            best_delta = distance
            ddG = delta_gibbs
            best_dG = dG
            best_sequence = seq
            best_ss = secondary_structure

    return (best_sequence.replace('U', 'T'), best_ss, best_dG)


def gene_optimizer(optimized_mRNA_leaders, orf_dict, proposed_aa_to_codons, wt_code):
    # attempts to resequence genes using new code to match same secondary structure
    # also tries to keep abundance of GATC sequences the same
    # todo: add in RBS model to keep those constant as well

    # nucleotide window to process. should be multiple of 3 (last set gets processed as is regardless of length.
    window = 15

    assert window % 3 == 0

    # insert RBS model here.

    # what about overlapping genes? internal rbs?

    workers, output_queue = rnafold_interface.generate_MT_workers(3)

    output_dict = {}

    seq_counter = 0

    for key in optimized_mRNA_leaders:

        leader = optimized_mRNA_leaders[key][0]
        gene_sequence = orf_dict[key][0]

        # replace leader of gene with modified, 'optimized' mRNA sequence
        updated_gene_sequence = leader + gene_sequence[len(leader):]
        updated_gene_sequence = updated_gene_sequence.replace('U', 'T')

        working_sequence = list(updated_gene_sequence)

        assert len(updated_gene_sequence) == len(gene_sequence)

        position = len(leader) + window

        subseq = working_sequence[position - window:position + window]
        wt_subseq = updated_gene_sequence[position - window:position + window]

        # iterate over the gene in 30 bp chunks (moving 3 bp/iteration)
        while (position <= len(updated_gene_sequence)):

            codons = [''.join(subseq[i:i + 3]) for i in range(0, len(subseq), 3)]
            proposed_new_seq = [proposed_aa_to_codons[wt_code[x]] for x in codons]
            seq_iterator = itertools.product(*proposed_new_seq)

            sequence_compiler = [wt_subseq]

            counter = 0
            for x in seq_iterator:
                if (counter % 250 == 0):
                    sequence_compiler.append(''.join(x))
                counter += 1

            # first result is WT sequence
            results = rnafold_interface.compute_structure_properties_MT(workers, output_queue, sequence_compiler)

            best_delta = len(results[0][0])
            ddG = 9999
            best_sequence = ''

            for (seq, secondary_structure, dG) in results:

                distance = hamming_distance(secondary_structure, results[0][1])
                delta_gibbs = (dG - results[0][2]) ** 2

                if (distance <= best_delta and delta_gibbs < ddG):
                    best_delta = distance
                    ddG = delta_gibbs
                    best_sequence = seq

            # update working sequence with new proposed
            working_sequence[position - window:position + window] = best_sequence.replace('U', 'T')

            if (position + window < len(working_sequence)):
                position += window
                subseq = working_sequence[position - window:position + window]
                wt_subseq = updated_gene_sequence[position - window:position + window]
            elif (position != len(working_sequence)):
                subseq = working_sequence[len(working_sequence) - window:len(working_sequence)]
                wt_subseq = updated_gene_sequence[len(working_sequence) - window:len(working_sequence)]
                position = len(working_sequence)
            else:
                break

        output_dict[key] = ''.join(working_sequence)

        print 'Finished sequence %i' % seq_counter
        seq_counter += 1

    for worker in workers:
        worker.terminate()

    return output_dict


def leading_sequence_optimizer(original_mRNA_dict, wt_code, proposed_aa_to_codons, novel_code):
    code_output = {}

    workers, output_queue = rnafold_interface.generate_MT_workers(3)

    sse = 0.0

    for key in original_mRNA_dict:

        subseq = original_mRNA_dict[key][0][7:]
        codons = [subseq[i:i + 3] for i in range(0, len(subseq), 3)]

        # the biggie: compute the cartesian product the defines the sequence using the new code

        proposed_new_seq = [proposed_aa_to_codons[wt_code[x]] for x in codons]

        seq_iterator = itertools.product(*proposed_new_seq)

        counter = 0

        sequence_compiler = []

        for x in seq_iterator:

            if (counter % 100 == 0):
                sequence_compiler.append(original_mRNA_dict[key][0][0:7] + ''.join(x))
            counter += 1

        results = rnafold_interface.compute_structure_properties_MT(workers, output_queue, sequence_compiler)

        best_delta = len(original_mRNA_dict[key][1])
        ddG = 9999
        best_sequence = ''

        best_ss = ''

        for (seq, secondary_structure, dG) in results:

            distance = hamming_distance(secondary_structure, original_mRNA_dict[key][1])
            delta_gibbs = (dG - original_mRNA_dict[key][2]) ** 2

            if (distance <= best_delta and delta_gibbs < ddG):
                best_delta = distance
                ddG = delta_gibbs
                best_sequence = seq
                best_ss = secondary_structure

        best_sequence = best_sequence.replace('U', 'T')

        (b_sequence, best_ss, best_dG) = tweak_whole_sequence(best_sequence, original_mRNA_dict[key][0],
                                                              proposed_aa_to_codons, novel_code, workers, output_queue,
                                                              starting_position=7)

        sse += (best_dG - original_mRNA_dict[key][2]) ** 2

        code_output[key] = (b_sequence[4:], best_ss, best_dG)

    for worker in workers:
        worker.terminate()

    return code_output


def execute(code_source):

    wt_code, _ = load_code(os.path.join(util.input_data_dir(),'Genetic Code EC.txt'))

    mRNA_sequences = load_fasta(os.path.join(util.output_sequence_dir(), 'truncated_essential_ecoli_orfs.fasta'))
    essential_orfs = load_fasta(os.path.join(util.input_data_dir(), 'essential_EColi_ORFs.fasta'))

    seq_ids = mRNA_sequences.keys()
    wt_sequences = [mRNA_sequences[x] for x in seq_ids]

    workers, queue = rnafold_interface.generate_MT_workers(3)

    structures = rnafold_interface.compute_structure_properties(wt_sequences)

    proper_mRNA_sequences = dict()

    for s, key in zip(structures, seq_ids):
        proper_mRNA_sequences[key] = (mRNA_sequences[key], s[1], s[2])

    essential_orfs_sequences = [essential_orfs[x] for x in seq_ids]

    structures = rnafold_interface.compute_structure_properties_MT(workers, queue, essential_orfs_sequences)

    for x in workers:
        x.terminate()

    proper_essential_orfs = dict()

    for s, key in zip(structures, seq_ids):
        proper_essential_orfs[key] = (essential_orfs[key], s[1], s[2])

    novel_codon_to_aa, proposed_aa_to_codons = load_code(os.path.join(util.output_sequence_dir(),code_source))

    output_tuple_dict = leading_sequence_optimizer(proper_mRNA_sequences, wt_code, proposed_aa_to_codons,
                                                   novel_codon_to_aa)

    revised_gene_dict = gene_optimizer(output_tuple_dict, proper_essential_orfs, proposed_aa_to_codons, wt_code)

    fhandle = open(os.path.join(util.output_sequence_dir(), 'truncated_essential_' + code_source.replace('.txt', '.fasta')), 'w')

    for key in output_tuple_dict:
        fhandle.write('>' + key.replace('>', '') + '\n')
        fhandle.write(output_tuple_dict[key][0] + '\n')
        fhandle.write(output_tuple_dict[key][1] + ' (' + str(output_tuple_dict[key][2]) + ')\n')

    fhandle.close()

    fhandle = open(os.path.join(util.output_sequence_dir(), 'revised_genes_') + code_source.replace('.txt', '.fasta'), 'w')

    for key in revised_gene_dict:
        fhandle.write('>' + key.replace('>', '') + '\n')
        fhandle.write(revised_gene_dict[key] + '\n')

    fhandle.close()

def driver():

    files = ['Fixed ATG, No Native Restriction, Distance Fitness Function.txt',
             'Fixed ATG, Native Restriction, Distance Fitness Function.txt',
             'Fixed ATG, Native Restriction, Linear Penalty for Changes Fitness Function.txt',
             'Fixed ATG, Native Restriction 2.0 power penalty for Changes Fitness Function.txt',
             'Ostgov Reassigned Strain Distance Fitness Function.txt',
             'RIC0.0.1.0.1.2.0.3.0.1.0.0.0.0.0.0.0.txt',
             'RIC0.1.1.0.1.2.0.3.0.0.0.0.0.0.0.0.0.txt']

    for f in files:
        execute(f)

if(__name__ == '__main__'):
    driver()

