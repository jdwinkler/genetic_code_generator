from collections import defaultdict
import os

def input_data_dir():

    return os.path.join(os.getcwd(),'input_sequences')

def output_sequence_dir():

    return os.path.join(os.getcwd(),'output_sequences')

def output_data_dir():

    return os.path.join(os.getcwd(),'data_output')

def load_fasta(file_path):

    fhandle = open(file_path, 'rU')
    lines = fhandle.readlines()
    fhandle.close()

    gene_sequence_dict = defaultdict(list)
    current_key = ''

    for i in range(0, len(lines)):

        line = lines[i].strip()

        if ('>' in line):
            current_key = line
        elif (current_key != ''):
            gene_sequence_dict[current_key].append(line)

    output = {}

    for key in gene_sequence_dict:
        output[key] = ''.join(gene_sequence_dict[key])

    return output
