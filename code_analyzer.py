__author__ = 'jdwinkler'

from collections import defaultdict
import numpy
import itertools
import os
import util
import sys

def aa_order():
    order = sorted(['H', 'K', 'R', 'D', 'E', 'S', 'T', 'N', 'Q', 'A', 'V', 'L', 'I', 'M', 'F', 'W', 'Y', 'P', 'G', 'C'])
    order.append('X')

    return order

def color_code():

    return range(1, len(aa_order())+1)

def load_genetic_code(filename, exclude_stop=False):
    fhandle = open(filename, 'rU')

    fhandle.readline()
    aa_to_codon = defaultdict(list)

    code = {}

    for line in fhandle:

        tokens = line.strip().split('\t')
        codon = tokens[0]
        aa = tokens[1]

        if (exclude_stop):
            if (aa != 'X'):
                code[codon] = aa
                aa_to_codon[aa].append(codon)

        else:
            code[codon] = aa
            aa_to_codon[aa].append(codon)

    codon_product = itertools.product(code.keys(), code.keys())

    one_transitions = defaultdict(int)

    observed_prev = set()

    for (codon_x, codon_y) in codon_product:

        if (hamming_distance(codon_x, codon_y) == 1 and code[codon_x] != code[codon_y]):

            if((codon_x, code[codon_x], code[codon_y]) not in observed_prev):
                one_transitions[codon_x] += 1
                observed_prev.add((codon_x, code[codon_x], code[codon_y]))

    fhandle.close()

    return code, aa_to_codon, one_transitions


def load_fasta(file_path):
    import os

    fhandle = open(file_path, 'rU')
    lines = fhandle.readlines()
    fhandle.close()

    gene_sequence_dict = defaultdict(list)
    current_key = ''

    for i in range(0, len(lines)):

        line = lines[i].strip()

        if ('>' in line):
            current_key = line
        elif (current_key != ''):
            gene_sequence_dict[current_key].append(line)

    output = {}

    for key in gene_sequence_dict:
        output[key] = ''.join(gene_sequence_dict[key])

    return output

def prepare_chemical_properties_dict(file_path, column, stop_value):

    temp_dict = {}

    with open(file_path,'rU') as fhandle:

        fhandle.readline()

        for line in fhandle:

            tokens = line.strip().split('\t')
            AA = tokens[0]
            chemical_class = tokens[column]

            temp_dict[AA] = chemical_class

        temp_dict['X'] = stop_value

    return temp_dict


def get_matrix(heatmap_data, x_keys,y_keys):
    matrix = []

    for key_y in y_keys:

        temp = []
        for key_x in x_keys:
            temp.append(heatmap_data[key_x][key_y])
        matrix.append(temp)

    matrix = numpy.array(matrix)

    return matrix


def multi_histogram(values, xlabel, ylabel, filename, legends=None, max_bin_norm=False):
    import matplotlib.pyplot as plt
    import matplotlib.patches as mpatches

    colors = [(0, 0, 1, 0.5),
              (1, 0, 0, 0.4),
              (0, 1, 1, 0.3),
              (0, 1, 0, 0.3),
              (0.7, 0.8, 0, 0.3),
              (0.4, 0.2, 0.8, 0.3)]

    try:

        fig, ax = plt.subplots()

        first_bins = []

        patches = []

        for i in range(0, len(values)):

            if (i == 0):
                hist, first_bins = numpy.histogram(values[i], normed=max_bin_norm)

            if (legends != None):
                patches.append(mpatches.Patch(color=colors[i], label=legends[i]))

            plt.hist(values[i], color=colors[i], bins=first_bins, normed=max_bin_norm)

        # plt.axis('tight')

        plt.legend(handles=patches, loc='upper left')

        plt.xlabel(xlabel)
        plt.ylabel(ylabel)

        plt.savefig(filename, bbox_inches='tight')
        plt.close('all')

    except:
        plt.close('all')
        raise


def hamming_distance(str1, str2):
    distance = 0
    if (len(str1) != len(str2)):
        raise AssertionError('unequal length in strings')

        # calculate hamming distance
    for base_x, base_y in zip(str1, str2):
        if (base_x != base_y):
            distance += 1

    return distance



def evolutionary_flexibility_scores(sequence_dict, transition_dict):

    scores = []
    keys = sequence_dict.keys()

    for key in keys:

        score = 0

        sequence = sequence_dict[key]

        for i in range(0, len(sequence), 3):
            codon = sequence[i:i + 3]
            score += transition_dict[codon]

        score = float(score) / (float(len(sequence)) / 3.0)

        scores.append(score)

    # print max_gene, max_flex

    return scores, keys


def heatmap(heatmap_data, x_keys, y_keys, filename, vmax=1, vmin=0):
    import matplotlib.pyplot as plt
    import matplotlib
    import numpy

    use_labels = True

    matrix = get_matrix(heatmap_data, x_keys,y_keys)

    try:

        fig, ax = plt.subplots()
        x_index = numpy.arange(len(x_keys))
        y_index = numpy.arange(len(y_keys))

        plt.figure(figsize=(4, 15))

        font = {'family': 'Bitstream Vera Sans',
                'weight': 'bold',
                'size': 12}

        matplotlib.rc('font', **font)

        plt.pcolormesh(matrix.transpose(), cmap='magma', vmax=vmax, vmin=vmin)

        cb = plt.colorbar()
        cb.set_ticks(color_code())

        cb_labels = aa_order()
        cb_labels.insert(0,'*')

        cb.set_ticklabels(cb_labels)

        plt.axis('tight')

        if (use_labels):
            plt.xticks(y_index + 0.5, y_keys, rotation='vertical')
            plt.yticks(x_index + 0.5, x_keys, rotation='horizontal')

            # ax.twinx().set_ylabel(ylabel)
            #cb.set_label('AA (codon)')

            # plt.xlabel(index, names, rotation='vertical')
            ax.set_yticklabels(x_keys, rotation='horizontal', va='center')

        plt.savefig(filename, bbox_inches='tight')
        plt.close('all')

    except:

        plt.close('all')
        raise


def aa_distance_calculations(wt_filename, filename, output_heatmap_name):

    code, aa_to_codon, one_transitions = load_genetic_code(filename)

    wt_code, _, _ = load_genetic_code(wt_filename)

    changes = sum([1 for x in wt_code if wt_code[x] != code[x]])

    print 'Number of changes from C_{i} to C_{f}: %i' % changes

    key_order = aa_order()

    if ('X' not in aa_to_codon):
        key_order.remove('X')

    codons = set()

    for aa_x in key_order:
        codons.update(aa_to_codon[aa_x])

    codon_one_distance = defaultdict(list)

    for codon_x in codons:
        for codon_y in codons:
            if(hamming_distance(codon_x,codon_y) == 1):
                codon_one_distance[codon_x].append(codon_y)

    heatmap_data = defaultdict(dict)

    color_dict = dict()

    for aa,color in zip(key_order, color_code()):
        color_dict[aa] = color

    for aa_x in key_order:

        aa_codons = aa_to_codon[aa_x]

        for c_x in aa_codons:
            counter = 1
            for c_y in codon_one_distance[c_x]:

                if(code[c_y] == aa_x):
                    heatmap_data[c_x][counter] = 0
                else:
                    heatmap_data[c_x][counter] = color_dict[code[c_y]]
                counter+=1

    print 'Minimum number of codons per aa', min([len(aa_to_codon[x]) for x in aa_to_codon if x != 'X'])
    print 'Maximum number of codons per aa', max([len(aa_to_codon[x]) for x in aa_to_codon if x != 'X'])

    heatmap(heatmap_data,
            sorted(list(codons)),
            range(1,10),
            output_heatmap_name,
            vmax=max(color_code()))

    return heatmap_data

def equivalent_sequence_analysis(wt_code):

    import decimal

    code, aa_to_codon,_ = load_genetic_code(wt_code)

    essential_orfs = load_fasta(os.path.join(util.input_data_dir(), 'essential_EColi_ORFs.fasta'))

    synonymous_library = []

    for id in essential_orfs:

        equivalent_permutations = 0

        sequence = essential_orfs[id]
        codons = [sequence[i:i+3] for i in range(0,len(sequence),3)]

        for codon in codons:
            equivalent_permutations += numpy.log(len(aa_to_codon[code[codon]]))

        synonymous_library.append(equivalent_permutations)

    print 'Average log of # combinations for equivalent library: %s (%s)' % (numpy.mean(synonymous_library), numpy.std(synonymous_library))

def plot_network(G, filename, shell1, shell2, use_node_weights = False, use_edge_weights = False,
                 use_labels = False, default_size = 300, scaling_factor = 1.0):

    import matplotlib.pyplot as plt

    import networkx

    scale = 15

    #pos = networkx.shell_layout(G, [shell1, shell2], scale=scale)
    pos = networkx.circular_layout(G, scale=scale)
    #pos = networkx.spring_layout(G, k = 0.1, iterations=250, scale=scale)

    node_color_test = []
    node_size_test = []

    for node in G.nodes():

        if('color' in G.node[node]):
            #node_colors[node] = G.node[node]['color']
            node_color_test.append(G.node[node]['color'])
        else:
            #default color is red (in pajek, matplotlib, etc)
            #node_colors[node] = 'r'
            node_color_test.append('r')

        if(use_node_weights and 'weight' in G.node[node]):
            node_size_test.append(max(G.node[node]['weight'] * scaling_factor,50))
        else:
            node_size_test.append(default_size)

    if(use_edge_weights == True):

        edge_weights = []

        for edge in G.edges():
            edge_weights.append(G[edge[0]][edge[1]]['weight'] * scaling_factor)

        networkx.draw_networkx_edges(G, pos = pos, edges = G.edges(), width=edge_weights)

    else:
        networkx.draw_networkx_edges(G, pos = pos)

    networkx.draw_networkx_nodes(G, pos = pos, node_color=node_color_test, node_size=node_size_test)

    if(use_labels == True):

        labels = {}

        for node in shell1:
            labels[node] = node

        networkx.draw_networkx_labels(G, pos = pos, labels = labels)

    plt.xticks([])
    plt.yticks([])

    plt.tight_layout()

    plt.savefig(filename)
    plt.close()

def output_node_weights(code_path, output_name):

    import networkx

    code, aa_to_codon, mut_one_step = load_genetic_code(code_path)

    mutated_edges = []

    aa_shell = set()
    codon_shell = set()

    hamming_dict = defaultdict(dict)

    cp = itertools.product(code.keys(), code.keys())
    for (x, y) in cp:
        hamming_dict[x][y] = hamming_distance(x, y)
        hamming_dict[y][x] = hamming_dict[x][y]

    codon_one_distance = defaultdict(set)

    for codon_x in hamming_dict:
        for codon_y in hamming_dict[codon_x]:
            if(hamming_dict[codon_x][codon_y] == 1):
                codon_one_distance[codon_x].add(codon_y)


    class_chem_dict = prepare_chemical_properties_dict(os.path.join(util.input_data_dir(),'aa_properties.txt'), -1, 'stop')

    for codon in code:
        #aa_shell.add(code[codon])
        codon_shell.add(codon)

        #mutated_edges.append((codon, code[codon]))

        for codon_y in codon_one_distance[codon]:
            mutated_edges.append((codon, codon_y))

    mutant_graph = networkx.Graph()
    mutant_graph.add_edges_from(mutated_edges)

    weights = []

    for (x, y) in mutated_edges:

        mutant_graph[x][y]['weight'] = 3 if (class_chem_dict[code[x]] != class_chem_dict[code[y]]) else 1
        weights.append((x, y, mutant_graph[x][y]['weight']))

    with open(output_name,'w') as f:

        f.write('\t'.join(['CodonX','CodonY','Weight']) + '\n')

        for (x,y,w) in weights:
            f.write('\t'.join(map(str,[x,y,w])) + '\n')

def generate_code_report(code_path, wt_code_path):

    print code_path

    class_chem_dict = prepare_chemical_properties_dict(os.path.join(util.input_data_dir(),'aa_properties.txt'), -1, 'stop')
    polar_chem_dict = prepare_chemical_properties_dict(os.path.join(util.input_data_dir(),'aa_properties.txt'), 2, 0.0)

    for x in polar_chem_dict:
        polar_chem_dict[x] = float(polar_chem_dict[x])

    codon_to_aa, aa_to_codon, _ = load_genetic_code(os.path.join(util.output_sequence_dir(),code_path))
    wt_codon_to_aa, wt_aa_to_codon, _ = load_genetic_code(os.path.join(util.output_sequence_dir(),wt_code_path))

    print 'Number of AAs in code (plus stop): %i' % len(aa_to_codon.keys())
    print 'Number of codons in code: %i' % len(codon_to_aa.keys())

    reassigned = 0

    for codon in codon_to_aa:
        if(wt_codon_to_aa[codon] != codon_to_aa[codon]):
            reassigned+=1

    print 'Number of reassigned codons: %i' % reassigned

    hamming_dict = defaultdict(dict)
    codon_one_distance = defaultdict(set)

    cp = itertools.product(codon_to_aa.keys(), codon_to_aa.keys())
    for (x, y) in cp:
        hamming_dict[x][y] = hamming_distance(x, y)
        hamming_dict[y][x] = hamming_dict[x][y]

    for codon_x in hamming_dict:
        for codon_y in hamming_dict[codon_x]:
            if(hamming_dict[codon_x][codon_y] == 1):
                codon_one_distance[codon_x].add(codon_y)

    class_differences = 0.0
    polarity_differences = 0.0

    unique_aa_score = 0.0

    aa_visit_score = defaultdict(int)

    for codon_x in codon_to_aa:
        unique_aa = set()
        for codon_y in codon_one_distance[codon_x]:

            if(codon_to_aa[codon_x] != codon_to_aa[codon_y]):
                unique_aa.add(codon_to_aa[codon_y])
                aa_visit_score[codon_to_aa[codon_y]]+=1
                if (class_chem_dict[codon_to_aa[codon_x]] != class_chem_dict[codon_to_aa[codon_y]]):
                    class_differences += 1
                polarity_differences += (polar_chem_dict[codon_to_aa[codon_x]] - polar_chem_dict[codon_to_aa[codon_y]])**2.0

        unique_aa_score += len(unique_aa)

    codons = float(len(codon_to_aa.keys()))

    visiting_score = float(min(aa_visit_score.values()))/float(max(aa_visit_score.values()))

    print 'Mean unique AAs accessible from a given codon by SNRs: %f' % (unique_aa_score/codons,)
    print 'Mean visits to each AA: %f' % visiting_score
    print 'Mean AAs with different classes accessible from a given codon: %f' % (float(class_differences) / codons,)
    print 'Mean square polarity differences accessible from a given codon: %f' % (float(polarity_differences) / codons,)

    print ''


def main():

    wt_code = 'Genetic Code EC.txt'
    #code,aa_to_codon,_ = load_genetic_code(os.path.join(util.output_sequence_dir(),wt_code),exclude_stop=True)

    files = ['Genetic Code EC.txt',
             'Fixed ATG, No Native Restriction, Distance Fitness Function.txt',
             'Fixed ATG, Native Restriction, Distance Fitness Function.txt',
             'Fixed ATG, Native Restriction, Linear Penalty for Changes Fitness Function.txt',
             'Fixed ATG, Native Restriction 2.0 power penalty for Changes Fitness Function.txt',
             'Ostgov Reassigned Strain Distance Fitness Function.txt',
             'RIC0.0.1.0.1.2.0.3.0.1.0.0.0.0.0.0.0.txt',
             'RIC0.1.1.0.1.2.0.3.0.0.0.0.0.0.0.0.0.txt']

    output_file_names = ['OriginalHeatMap',
                         'No Native Restriction, Fitness minimized HeatMap',
                         'Native Restriction, Fitness minimized HeatMap',
                         'Native Restriction, Changes Minimized HeatMap',
                         'Native Restriction, Changes Minimized Power 2.0 HeatMap',
                         'Ostgov Reassigned Strain Distance Fitness Function',
                         'RIC0.0.1.0.1.2.0.3.0.1.0.0.0.0.0.0.0',
                         'RIC0.1.1.0.1.2.0.3.0.0.0.0.0.0.0.0.0']

    sequences = [os.path.join(util.output_sequence_dir(), 'revised_genes_' + x.replace('.txt', '.fasta')) for x
                 in files]

    count = 0

    for file_x, output_name in zip(files, output_file_names):
        generate_code_report(os.path.join(util.output_sequence_dir(),file_x),
                             os.path.join(util.output_sequence_dir(),wt_code))

        output_node_weights(os.path.join(util.output_sequence_dir(),file_x),
                            os.path.join(util.output_data_dir(), 'nw_' + file_x))


        count+=1

    scores_for_histogram = []

    for file_x, sequence in zip(files, sequences):

        code, _, transition_dict = load_genetic_code(os.path.join(util.output_sequence_dir(),file_x))

        loaded_sequence = load_fasta(sequence)

        scores, _ = evolutionary_flexibility_scores(loaded_sequence, transition_dict)

        scores_for_histogram.append(scores)

    fhandle = open(os.path.join(util.output_data_dir(),'histogram_scores.txt'), 'w')
    fhandle.write('\t'.join(['WT', 'BTC', 'NR_BTC', 'CMC', 'CMC**2.0', 'Ostgov', 'RIC0','RIC1']) + '\n')

    for i in range(0, len(scores_for_histogram[0])):
        output = [str(v[i]) for v in scores_for_histogram]
        fhandle.write('\t'.join(output) + '\n')

    fhandle.close()


if (__name__ == '__main__'):
    main()
