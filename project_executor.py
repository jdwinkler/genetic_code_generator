import genetic_code_GA

genetic_code_GA.driver()

import secondary_structure_optimization

#warning: this will take a considerable amount of time, something like 20 hours

try:
    secondary_structure_optimization.driver()
except:
    print 'Does the current directory contain rnafold?'
    raise

import code_analyzer

code_analyzer.main()